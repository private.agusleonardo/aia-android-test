package com.aia.android

import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun testArray() {
        val mutableList: MutableList<String> = arrayListOf()
        if(mutableList.isNullOrEmpty())
        mutableList.add(0,"0")
        mutableList.add(1,"1")
        mutableList.add(2,"2")
        mutableList.add(3,"3")
        mutableList.removeAt(1)

        print(mutableList)
    }
}
