package com.aia.android

object Constants {
    const val BaseURL = "https://aia-backend-test.herokuapp.com/"

    const val GALLERY_IMAGE = 18
    const val REQUEST_TAKE_PHOTO = 97
}