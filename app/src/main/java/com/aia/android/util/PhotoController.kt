package com.aia.android.util

import android.Manifest
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import com.aia.android.Constants
import dagger.android.support.DaggerAppCompatActivity
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

open class PhotoController(val activity: Activity, val callback: PhotoInterface) {
    private val PERMISSION_CODE = 10

    private val PERMISSION_NEEDED = arrayOf(
        Manifest.permission.CAMERA,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE
    )

    fun takePhoto(activity: DaggerAppCompatActivity) {
        if (checkPermit()) {
            dispatchTakePictureIntent(activity)
        }
    }

    fun dialogAttachment() {
        val items_category = ArrayList<String>()
        items_category.add("Camera")
        items_category.add("Gallery")

        val editmenu = items_category.toTypedArray()
        val builder = AlertDialog.Builder(activity)
        builder.setTitle(null)
        builder.setItems(editmenu, object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface, item: Int) {
                dialog.dismiss()
                when (item) {
                    0 -> {
                        if (checkPermit()) {
                            dispatchTakePictureIntent()
                        }
                    }
                    1 -> {
                        if (checkPermit()) {
                            val intent = Intent(Intent.ACTION_PICK)
                            intent.type = "image/*"
                            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE,true)
                            activity.startActivityForResult(
                                Intent.createChooser(intent, "Select Picture"),
                                Constants.GALLERY_IMAGE
                            )
                        }
                    }

                }
            }
        })
        val alert = builder.create()
        alert.show()
    }

    private fun dispatchTakePictureIntent(activity: DaggerAppCompatActivity) {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            takePictureIntent.resolveActivity(activity.packageManager)?.also {
                // Create the File where the photo should go
                val photoFile: File? = try {
                    createImageFile()
                } catch (ex: IOException) {
                    // Error occurred while creating the File
                    ex.printStackTrace()
                    null
                }
                // Continue only if the File was successfully created
                photoFile?.also {
                    val photoURI: Uri = when {
                        Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT -> Uri.fromFile(it)
                        else -> FileProvider.getUriForFile(
                            activity,
                            "com.aia.android.fileprovider",
                            it
                        )
                    }
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    activity.startActivityForResult(takePictureIntent, Constants.REQUEST_TAKE_PHOTO)
                }
            }
        }
    }

    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            takePictureIntent.resolveActivity(activity.packageManager)?.also {
                // Create the File where the photo should go
                val photoFile: File? = try {
                    createImageFile()
                } catch (ex: IOException) {
                    // Error occurred while creating the File
                    ex.printStackTrace()
                    null
                }
                // Continue only if the File was successfully created
                photoFile?.also {
                    val photoURI: Uri = when {
                        Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT -> Uri.fromFile(it)
                        else -> FileProvider.getUriForFile(
                            activity,
                            "com.aia.android.fileprovider",
                            it
                        )
                    }
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    activity.startActivityForResult(takePictureIntent, Constants.REQUEST_TAKE_PHOTO)
                }
            }
        }
    }

    private fun checkPermit(): Boolean {
        var permissionRequest: Array<String> = arrayOf()

        for (String in PERMISSION_NEEDED) {
            if (ActivityCompat.checkSelfPermission(
                    activity,
                    String
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                permissionRequest += String
            }
        }

        if (permissionRequest.isNotEmpty()) {
            ActivityCompat.requestPermissions(activity, permissionRequest, PERMISSION_CODE)
        } else {
            return true
        }
        return false
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File? = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            callback.path(absolutePath)
        }
    }
}