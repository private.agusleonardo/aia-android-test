package com.aia.android.util

import androidx.annotation.NonNull
import io.reactivex.Scheduler

interface ISchedulerProvider {

    @NonNull
    fun computation(): Scheduler

    @NonNull
    fun ui(): Scheduler
}