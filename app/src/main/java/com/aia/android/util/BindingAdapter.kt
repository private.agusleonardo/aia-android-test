package com.aia.android.util

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.aia.android.R

object BindingAdapter {
    @JvmStatic
    @BindingAdapter("addSrc")
    fun addSrc(view: ImageView, show: Int) {
        view.setBackgroundResource(show)
    }

    @BindingAdapter("addGlideImage")
    @JvmStatic
    fun addGlideImage(view: ImageView, txt: String?) {
        GlideApp.with(view.context)
            .load(txt)
            .error(R.drawable.ic_account)
            .into(view)
    }

}