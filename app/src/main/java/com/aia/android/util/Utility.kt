package com.aia.android.util

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.os.Build
import android.text.Html
import android.text.Spanned
import android.util.Log
import android.view.ContextThemeWrapper
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AlertDialog
import com.aia.android.R
import kotlinx.android.synthetic.main.dialog_custom_check.*
import java.text.SimpleDateFormat
import java.util.*

object Utility {
    private val TAG = "Utility"
    private val currentDate = Calendar.getInstance().time

    fun log(message: String) {
        Log.d("Utility", message)
    }

    fun convertStringToDate(type: Int, dateTxt: String): Date? {
        val timezone = TimeZone.getDefault().getDisplayName(false, TimeZone.SHORT)
        val parser = when (type) {
            1 -> SimpleDateFormat("E MMM dd HH:mm:ss $timezone yyyy", Locale.ENGLISH)
            2 -> SimpleDateFormat("dd MM yyyy HH:mm:ss", Locale.ENGLISH)
            3 -> SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
            4 -> SimpleDateFormat("MM", Locale.ENGLISH)
            5 -> SimpleDateFormat("MMMM", Locale.getDefault())
            6 -> SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
            7 -> SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH)
            8 -> SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault())
            else -> SimpleDateFormat("dd MM yyyy HH:mm:ss", Locale.ENGLISH)
        }
        return parser.parse(dateTxt)
    }

    fun convertDateToString(type: Int, date: Date): String {
        val df = when (type) {
            1 -> SimpleDateFormat("E MMM dd HH:mm:ss z yyyy", Locale.ENGLISH)
            2 -> SimpleDateFormat("dd MM yyyy HH:mm:ss", Locale.ENGLISH)
            3 -> SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
            4 -> SimpleDateFormat("MM", Locale.ENGLISH)
            5 -> SimpleDateFormat("MMMM", Locale.getDefault())
            6 -> SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
            7 -> SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH)
            8 -> SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)
            9 -> SimpleDateFormat("HH:mm", Locale.ENGLISH)
            10 -> SimpleDateFormat("d/M/yyyy", Locale.ENGLISH)
            11 -> SimpleDateFormat("d M yyyy",Locale.ENGLISH)
            else -> SimpleDateFormat("dd MM yyyy HH:mm:ss", Locale.ENGLISH)
        }
        return df.format(date)
    }

    fun convertStringToDateWithTimezone(type: Int, dateTxt: String): Date? {
        val parser = when (type) {
            1 -> SimpleDateFormat(
                "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
                Locale.getDefault()
            )
            else -> SimpleDateFormat(
                "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
                Locale.getDefault()
            )
        }
        parser.timeZone = TimeZone.getTimeZone("GMT")
        return parser.parse(dateTxt)
    }

    fun fromHtml(html: String?): Spanned {
        return when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.N -> Html.fromHtml(
                html!!,
                Html.FROM_HTML_MODE_LEGACY
            )
            else -> Html.fromHtml(html)
        }
    }

    fun showDialog(context: Context, title: String, message: String, type: String) {
        val dialog: AlertDialog
        val builder = AlertDialog.Builder(context)
        builder.setTitle(title)
        builder.setMessage(message)
        builder.setPositiveButton("OK") { dialogInterface, _ ->
            dialogInterface.dismiss()
        }
        dialog = builder.create()
        dialog.show()
        dialog.setCanceledOnTouchOutside(false)
    }

    fun showCustomDialog(
        context: Context,
        type: Boolean,
        message: String,
        clickCallback: ((AlertDialog) -> Unit)?
    ): AlertDialog {
        val dialog: AlertDialog
        val builder = AlertDialog.Builder(context).apply {
            setView(
                LayoutInflater.from(context).inflate(
                    if (type) R.layout.dialog_custom_check else R.layout.dialog_custom_cross,
                    null
                )
            )
        }
        dialog = builder.create()
        dialog.show()
        dialog.tv_text.text = message
        dialog.tv_clickOk.setOnClickListener {
            clickCallback?.invoke(dialog)
        }
        dialog.setCanceledOnTouchOutside(false)
        return dialog
    }

//    fun showDialog(context: Context, title: String, message: String, yes: () -> Unit, no: () -> Unit) {
//        val dialog: AlertDialog
//        val builder = AlertDialog.Builder(context)
//        builder.setTitle(title)
//        builder.setMessage(message)
//        builder.setPositiveButton("Yes") { dialogInterface, _ ->
//            yes()
//            dialogInterface.dismiss()
//        }
//        builder.setNegativeButton("No") { dialogInterface, _ ->
//            no()
//            dialogInterface.dismiss()
//        }
//        dialog = builder.create()
//        dialog.show()
//        dialog.setCanceledOnTouchOutside(false)
//    }

//    fun initRecyclerViewSkeleton(
//        recyclerView: RecyclerView,
//        adapter: Adapter<RecyclerView.ViewHolder>?,
//        skeletonLayout: Int
//    ): RecyclerViewSkeletonScreen {
//        return Skeleton.bind(recyclerView)
//            .duration(1500)
//            .adapter(adapter)
//            .angle(0)
//            .color(R.color.grey_200)
//            .load(skeletonLayout)
//            .show()
//    }

//    fun initViewSkeleton(rootView: View, skeletonLayout: Int): ViewSkeletonScreen {
//        return Skeleton.bind(rootView)
//            .color(R.color.shimmer_color)
//            .load(skeletonLayout)
//            .show()
//    }

    fun hideKeyboard(activity: Activity) {
        val imm =
            activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view: View? = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    @SuppressLint("PrivateResource")
    fun dialogLoading(activity: Activity): AlertDialog {
        val buildersold = AlertDialog.Builder(activity, R.style.CustomDialog)
        val alertViewsold =
            View.inflate(
                ContextThemeWrapper(activity, R.style.ThemeOverlay_AppCompat_Dialog_Alert),
                R.layout.dialog_loading,
                null
            )
        buildersold.setView(alertViewsold)
        buildersold.setTitle(null)

        val mAlertDialogsold = buildersold.create()
        mAlertDialogsold.setCancelable(false)
        return mAlertDialogsold
    }

}