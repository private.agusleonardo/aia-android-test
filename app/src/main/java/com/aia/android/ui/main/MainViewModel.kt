package com.aia.android.ui.main

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.aia.android.api.model.Resource
import com.aia.android.cache.PreferencesHelper
import com.aia.android.model.FlickrData
import com.aia.android.repository.UserRepository
import com.aia.android.ui.base.BaseViewModel
import javax.inject.Inject

class MainViewModel @Inject constructor(
    val userRepository: UserRepository,
    val preferencesHelper: PreferencesHelper
) : BaseViewModel() {
    var name = ObservableField("")
    var tags = ObservableField("")
    var checkTrigger: MutableLiveData<Boolean> = MutableLiveData()

    fun updateProfile() {
        name.set(preferencesHelper.getAccountRx().blockingGet().username)
    }

    var check : LiveData<Resource<FlickrData>> = Transformations.switchMap(checkTrigger) {
        userRepository.getFlickrPicture(tags.get()?:"")
    }

    fun getFlickrPicture(){
        checkTrigger.value = checkTrigger.value == false
    }

}