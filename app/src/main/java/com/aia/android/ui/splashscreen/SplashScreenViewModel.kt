package com.aia.android.ui.splashscreen

import com.aia.android.cache.PreferencesHelper
import com.aia.android.ui.base.BaseViewModel
import javax.inject.Inject

class SplashScreenViewModel @Inject constructor(
    val preferencesHelper: PreferencesHelper
) : BaseViewModel()