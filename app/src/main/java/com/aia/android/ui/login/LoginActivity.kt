package com.aia.android.ui.login

import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.aia.android.R
import com.aia.android.api.model.Status
import com.aia.android.databinding.ActivityLoginBinding
import com.aia.android.ui.base.BaseActivity
import com.aia.android.ui.main.MainActivity
import javax.inject.Inject

class LoginActivity : BaseActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: LoginViewModel

    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        viewModel = initViewModel(LoginViewModel::class.java, viewModelFactory)
        binding.viewModel = viewModel

        initObserver()
        initListener()
    }

    private fun initListener() {
        binding.etPass.setOnKeyListener { v, keyCode, event ->
            when (keyCode) {
                KeyEvent.KEYCODE_ENTER -> loginCheck()
            }
            false
        }
        binding.btLogin.setOnClickListener {
            loginCheck()
        }
    }

    private fun loginCheck() {
        if (!binding.etUsername.text.isNullOrEmpty() || !binding.etPass.text.isNullOrEmpty()) {
            viewModel.username.set(binding.etUsername.text.toString())
            viewModel.pass.set(binding.etPass.text.toString())
            viewModel.login()
        } else Toast.makeText(
            this,
            "Email dan Password tidak boleh kosong",
            Toast.LENGTH_SHORT
        ).show()
    }

    private fun initObserver() {
        viewModel.check.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    if (it.data != null) {
                        startActivity(Intent(this, MainActivity::class.java))
                        finish()
                    }
                }
                Status.VALIDATION_ERROR -> {
                    Toast.makeText(this, "Email / Password salah", Toast.LENGTH_SHORT).show()
                }
                Status.ERROR -> {
                    Toast.makeText(this, "Terjadi kesalahan", Toast.LENGTH_SHORT).show()
                }
            }
        })
    }
}