package com.aia.android.ui.splashscreen

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.aia.android.R
import com.aia.android.ui.base.BaseActivity
import com.aia.android.ui.login.LoginActivity
import com.aia.android.ui.main.MainActivity
import javax.inject.Inject

class SplashScreenActivity : BaseActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: SplashScreenViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splashscreen)
        viewModel = initViewModel(SplashScreenViewModel::class.java, viewModelFactory)

        if (viewModel.preferencesHelper.rememberMe) startActivity(Intent(this, MainActivity::class.java))
        else startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }

}