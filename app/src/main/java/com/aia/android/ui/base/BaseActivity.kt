package com.aia.android.ui.base

import android.app.Activity
import android.app.Dialog
import android.os.Bundle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.aia.android.util.Utility
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.toolbar.*

abstract class BaseActivity : DaggerAppCompatActivity() {

    private lateinit var dialog: Dialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
    fun setUpDialog(activity: Activity){
        dialog = Utility.dialogLoading(activity)
    }

    fun showDialog(){
        dialog.show()
    }
    fun dismissDialog(){
        dialog.dismiss()
    }

    fun setToolbar(title: String, backAction: Boolean) {
        setSupportActionBar(toolbar)
        supportActionBar!!.title = title
        if (backAction) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
            toolbar.setNavigationOnClickListener {
                onBackPressed()
            }
        }
    }

    fun <T : ViewModel?> initViewModel(
        modelClass: Class<T>,
        isNeedFactory: ViewModelProvider.Factory?
    ): T {
        return if (isNeedFactory != null)
            ViewModelProviders.of(this, isNeedFactory).get(modelClass)
        else
            ViewModelProviders.of(this).get(modelClass)
    }


}