package com.aia.android.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aia.android.databinding.ListItemMenuBinding
import com.aia.android.model.FlickrPictureData

class MainAdapter : RecyclerView.Adapter<MainAdapter.ItemViewHolder>() {

    var data: MutableList<FlickrPictureData> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(
            ListItemMenuBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(data[position])
    }

    inner class ItemViewHolder(itemView: ListItemMenuBinding) :
        RecyclerView.ViewHolder(itemView.root) {
        val binding = itemView
        fun bind(data: FlickrPictureData) {
            binding.menuData = data
        }

    }

    fun setMenuData(data: MutableList<FlickrPictureData>) {
        this.data = data
        notifyDataSetChanged()
    }

}
