package com.aia.android.ui.main

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.core.widget.addTextChangedListener
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.aia.android.R
import com.aia.android.databinding.ActivityMainBinding
import com.aia.android.model.FlickrPictureData
import com.aia.android.model.UserData
import com.aia.android.ui.base.BaseActivity
import javax.inject.Inject

class MainActivity : BaseActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: MainViewModel
    private lateinit var binding: ActivityMainBinding
    lateinit var adapter: MainAdapter

    lateinit var userData: UserData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        viewModel = initViewModel(MainViewModel::class.java, viewModelFactory)
        binding.viewModel = viewModel

        viewModel.updateProfile()
        setToolbar("Hi ${viewModel.name.get()} !", false)

        adapter = MainAdapter()

        initProfile()
        initObserver()
        initAdapter()
        initListener()
        viewModel.getFlickrPicture()
    }

    private fun initListener() {
        binding.etSearch.addTextChangedListener {
            viewModel.tags.set(binding.etSearch.text.toString())
            viewModel.getFlickrPicture()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_search, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.search -> {
                if (binding.layoutSearch.visibility == View.GONE)
                    binding.layoutSearch.visibility = View.VISIBLE
                else binding.layoutSearch.visibility = View.GONE
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initProfile() {
        userData = viewModel.preferencesHelper.getAccountRx().blockingGet()
    }

    private fun initObserver() {
        viewModel.check.observe(this, Observer {
            if (it.data != null){
                adapter.setMenuData(it.data.items as MutableList<FlickrPictureData>)
                adapter.notifyDataSetChanged()
            }
        })
    }

    private fun initAdapter() {
        binding.menuList.layoutManager = LinearLayoutManager(this)
        binding.menuList.adapter = adapter
    }

    override fun onResume() {
        super.onResume()
        viewModel.updateProfile()
    }

}
