package com.aia.android.api.model

enum class Status {
    SUCCESS,
    ERROR,
    ACCEPTED,
    UNAUTH,
    LOADING,
    VALIDATION_ERROR
}