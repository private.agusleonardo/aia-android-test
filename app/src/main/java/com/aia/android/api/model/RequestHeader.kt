package com.aia.android.api.model

data class RequestHeader(
    var accesstoken: AccessToken,
    val language: String,
    var employeeId: String
)