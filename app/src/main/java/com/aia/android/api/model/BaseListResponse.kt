package com.aia.android.api.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class BaseListResponse<T>(
    @SerializedName("status")
    @Expose
    val statusCode: Boolean,
    @SerializedName("messages")
    @Expose
    var message: List<String>? = ArrayList(),
    @SerializedName("data")
    @Expose
    var data: List<T> = ArrayList()
)