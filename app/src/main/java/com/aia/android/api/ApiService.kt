package com.aia.android.api

import androidx.lifecycle.LiveData
import com.aia.android.api.model.BaseDataResponse
import com.aia.android.api.model.BaseResponse
import com.aia.android.model.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*


interface ApiService {
    @FormUrlEncoded
    @POST("login")
    fun login(@FieldMap params: Map<String?, String?>?): LiveData<ApiResponse<BaseDataResponse<LoginData>>>

    @FormUrlEncoded
    @POST("picture")
    fun getFlickrPicture(@FieldMap params: Map<String?, String?>?): LiveData<ApiResponse<FlickrData>>

}