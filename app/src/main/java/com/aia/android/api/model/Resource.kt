package com.aia.android.api.model

data class Resource<out T>(val status: Status, val data: T?, val message: String?) {
    companion object {
        fun <T> success(data: T?): Resource<T> {
            return Resource(Status.SUCCESS, data, null)
        }

        fun <T> error(msg: String, data: T?): Resource<T> {
            return Resource(Status.ERROR, data, msg)
        }

        fun <T> accepted(): Resource<T> {
            return Resource(Status.ACCEPTED, null, null)
        }

        fun <T> unauthorized(): Resource<T> {
            return Resource(Status.UNAUTH, null, null)
        }

        fun <T> loading(data: T?): Resource<T> {
            return Resource(Status.LOADING, data, null)
        }

        fun <T> validationError(): Resource<T> {
            return Resource(Status.VALIDATION_ERROR, null, null)
        }
    }
}