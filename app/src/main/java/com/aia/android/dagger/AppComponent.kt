package com.aia.android.dagger

import com.aia.android.AIAAndroidApp
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        AppModule::class
    ]
)
interface AppComponent : AndroidInjector<AIAAndroidApp> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<AIAAndroidApp>()
}