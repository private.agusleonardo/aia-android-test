package com.aia.android.model

import com.google.gson.annotations.SerializedName

data class FlickrPictureData(
    @SerializedName("title")
    var title: String? = "",
    @SerializedName("description")
    var description: String? = "",
    @SerializedName("media")
    var media: MediaData
)