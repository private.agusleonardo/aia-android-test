package com.aia.android.model

import com.google.gson.annotations.SerializedName

data class FlickrData(
    @SerializedName("items")
    var items: List<FlickrPictureData>

)