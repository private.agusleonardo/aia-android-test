package com.aia.android.model

import com.google.gson.annotations.SerializedName

data class MediaData(
    @SerializedName("m")
    var imageUrl: String? = ""
)