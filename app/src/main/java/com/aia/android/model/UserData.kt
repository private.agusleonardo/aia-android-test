package com.aia.android.model

import com.google.gson.annotations.SerializedName

data class UserData(
    @SerializedName("username")
    var username: String? =""
)