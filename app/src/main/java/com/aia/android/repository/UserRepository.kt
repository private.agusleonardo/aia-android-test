package com.aia.android.repository

import androidx.lifecycle.LiveData
import com.aia.android.api.ApiService
import com.aia.android.api.ApiSuccessResponse
import com.aia.android.api.model.BaseDataResponse
import com.aia.android.api.model.BaseResponse
import com.aia.android.api.model.RequestHeader
import com.aia.android.api.model.Resource
import com.aia.android.cache.PreferencesHelper
import com.aia.android.model.*
import com.aia.android.util.AppExecutors
import com.aia.android.util.NetworkBoundResource
import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserRepository @Inject constructor(
    private val appExecutors: AppExecutors,
    private val preferencesHelper: PreferencesHelper,
    private val APIService: ApiService,
    private val requestHeaders: RequestHeader,
    private val localUserData: LocalUserData
) {

    fun getLocalUserData(): Single<UserData> {
        return preferencesHelper.getAccountRx()
    }

    fun login(
        userId: String,
        password: String
    ): LiveData<Resource<LoginData>> {
        return object : NetworkBoundResource<LoginData, BaseDataResponse<LoginData>>(appExecutors) {
            override fun processResult(item: BaseDataResponse<LoginData>): LoginData? {
                preferencesHelper.saveAccount(item.data!!.user)
                return item.data
            }

            override fun createCall() =
                APIService.login(
                    loginParams(userId, password)
                )

            override fun processResponse(response: ApiSuccessResponse<BaseDataResponse<LoginData>>)
                    : BaseDataResponse<LoginData> {
                val body = response.body
                return body
            }
        }.asLiveData()
    }

    private fun loginParams(
        userId: String?,
        password: String?
    ): Map<String?, String?>? {
        val params = HashMap<String?, String?>()
        params["username"] = userId
        params["password"] = password
        return params
    }

    fun getFlickrPicture(tags:String): LiveData<Resource<FlickrData>> {
        return object :
            NetworkBoundResource<FlickrData, FlickrData>(appExecutors) {
            override fun processResult(item: FlickrData): FlickrData? {
                return item
            }

            override fun createCall() =
                APIService.getFlickrPicture(flickrParams(tags))

            override fun processResponse(response: ApiSuccessResponse<FlickrData>)
                    : FlickrData {
                val body = response.body
                return body
            }
        }.asLiveData()
    }

    private fun flickrParams(
        tags: String?
    ): Map<String?, String?>? {
        val params = HashMap<String?, String?>()
        params["tags"] = tags
        return params
    }

}