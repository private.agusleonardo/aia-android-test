package com.aia.android.cache

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import com.aia.android.model.UserData
import com.aia.android.ui.login.LoginActivity
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PreferencesHelper @Inject constructor(private val context: Context) {
    companion object {
        private const val keyPackageName = "aia"
        private const val keyUserName = "userName"
        private const val keyRememberMe = "rememberMe"
    }

    private val atPref: SharedPreferences =
        context.getSharedPreferences(keyPackageName, Context.MODE_PRIVATE)

    var rememberMe: Boolean
        get() = atPref.getBoolean(keyRememberMe, false)
        set(rememberMe) = atPref.edit().putBoolean(keyRememberMe, rememberMe).apply()

    fun saveAccount(userData: UserData) {
        atPref.edit().putBoolean(keyRememberMe, true).apply()
        atPref.edit().putString(keyUserName, userData.username ?: "").apply()
    }

    fun getAccountRx(): Single<UserData> {
        val user = UserData(
            username = atPref.getString(keyUserName, null)
        )
        return Single.just(user)
    }

}